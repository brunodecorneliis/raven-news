package com.raven.news

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.raven.home.presentation.view.catalog.NewsCatalogActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        NewsCatalogActivity.start(this)
        finish()
    }
}
