package com.raven.core

import android.view.View
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun launch(call: suspend CoroutineScope.() -> Unit) {
    runBlocking { // this: CoroutineScope
        launch { call() }
    }
}

fun View.visibleOrGone(visible: Boolean) {
    visibility = if (visible) {
        View.VISIBLE
    } else {
        View.GONE
    }
}

fun View.gone() {
    visibility = View.GONE
}