plugins {
    id(ModulePlugin.MODULE_NAME)
}

android {
    namespace = "com.raven.network"
}

dependencies {
    implementation(project(":feature:home"))
    di()
    general()
    network()
}
