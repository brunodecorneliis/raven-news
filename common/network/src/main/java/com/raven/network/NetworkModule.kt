package com.raven.network

import com.raven.home.data.remote.service.HomeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        OkHttpClient.Builder().apply {
            addInterceptor(loggingInterceptor)
            addInterceptor(Interceptor { chain ->
                val original = chain.request()
                val url = original.url.newBuilder()
                    .addQueryParameter(API_PARAMETER, API_KEY)
                    .build()
                chain.proceed(original.newBuilder().url(url).build())

            })
            return this.build()
        }
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder().baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build()
    }

    @Provides
    fun provideTrackService(retrofit: Retrofit): HomeService {
        return retrofit.create(HomeService::class.java)
    }

    companion object {
        private const val BASE_URL = "https://api.nytimes.com/svc/mostpopular/v2/"
        private const val API_PARAMETER = "api-key"
        private const val API_KEY = "V4Me99XOuOwdxrTCwsOR10HhqWOlRdLO"
    }
}
