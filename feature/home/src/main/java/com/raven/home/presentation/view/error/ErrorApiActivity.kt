package com.raven.home.presentation.view.error

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.raven.core.bases.BaseActivity
import com.raven.home.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
open class ErrorApiActivity : BaseActivity() {
    override fun init() {
        hideToolbar()
        replaceFragment(ErrorApiFragment.newInstance())
    }

    companion object {
        fun start(context: Context) {
            with(Intent(context, ErrorApiActivity::class.java)) {
                addFlags(FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(this)
            }
        }
    }
}