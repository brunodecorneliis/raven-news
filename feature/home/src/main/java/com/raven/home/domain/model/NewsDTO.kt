
package com.raven.home.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsDTO(
    @SerializedName("type") 
    val type: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("abstract")
    val abstract: String?,
    @SerializedName("published_date")
    val publishedDate: String?,
    @SerializedName("source")
    val source: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("byline")
    val byline: String?,
    @SerializedName("media")
    val media: List<NewsMediaDTO>
): Parcelable