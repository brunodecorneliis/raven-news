package com.raven.home.presentation.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BindingViewHolder<ItemType, out VB : ViewDataBinding>(val binding: VB) :
    RecyclerView.ViewHolder(binding.root) {

    abstract fun bind(item: ItemType)
}