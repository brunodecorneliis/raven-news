package com.raven.home.presentation.view.detail

import android.content.Context
import android.content.Intent
import com.raven.core.bases.BaseActivity
import com.raven.home.domain.model.NewsDTO
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsDetailActivity : BaseActivity() {

    override fun init() {
        replaceFragment(
            NewsDetailFragment.newInstance(intent.getParcelableExtra(EXTRA_NEWS))
        )
    }

    companion object {
        private const val EXTRA_NEWS = "extraNews"

        fun start(
            context: Context,
            news: NewsDTO
        ) {
            with(Intent(context, NewsDetailActivity::class.java)) {
                putExtra(EXTRA_NEWS, news)
                context.startActivity(this)
            }
        }
    }
}