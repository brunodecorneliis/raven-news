package com.raven.home.presentation.view.error

import com.raven.core.bases.BaseFragment
import com.raven.home.R
import com.raven.home.databinding.FragmentErrorBinding

class ErrorApiFragment : BaseFragment<FragmentErrorBinding>() {

    override fun layout() = R.layout.fragment_error

    override fun init() {}

    companion object {
        fun newInstance(): ErrorApiFragment {
            return ErrorApiFragment()
        }
    }
}