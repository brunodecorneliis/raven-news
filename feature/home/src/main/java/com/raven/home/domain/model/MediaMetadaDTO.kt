package com.raven.home.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MediaMetadaDTO(
    @SerializedName("url")
    val url: String?
) : Parcelable