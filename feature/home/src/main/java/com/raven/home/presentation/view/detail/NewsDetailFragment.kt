package com.raven.home.presentation.view.detail

import android.graphics.drawable.Drawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.raven.core.bases.BaseFragment
import com.raven.home.R
import com.raven.home.databinding.FragmentDetailNewsBinding
import com.raven.home.domain.model.NewsDTO

class NewsDetailFragment : BaseFragment<FragmentDetailNewsBinding>() {

    lateinit var news: NewsDTO

    override fun layout() = R.layout.fragment_detail_news

    override fun init() {
        setView()
    }

    private fun setView() {
        with(binding) {
            title.text = news.title
            subtitle.text = news.abstract
            date.text = news.publishedDate
            author.text = news.byline
            Glide.with(image)
                .load(news.media.first().mediaMetadata.first().url)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any,
                        target: Target<Drawable>,
                        isFirstResource: Boolean
                    ): Boolean {
                        image.setImageResource(R.drawable.note_off_outline)
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        model: Any,
                        target: Target<Drawable>,
                        dataSource: DataSource,
                        isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                }).into(image)
        }
    }

    companion object {
        fun newInstance(news: NewsDTO?): NewsDetailFragment {
            return NewsDetailFragment().apply {
                news?.let { this.news = it }
            }
        }
    }
}