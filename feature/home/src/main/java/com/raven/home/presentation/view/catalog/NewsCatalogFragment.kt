package com.raven.home.presentation.view.catalog

import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.raven.core.bases.BaseFragment
import com.raven.core.visibleOrGone
import com.raven.home.R
import com.raven.home.databinding.NewsCatalogFragmentBinding
import com.raven.home.domain.model.NewsDTO
import com.raven.home.presentation.adapter.NewsAdapter
import com.raven.home.presentation.view.error.ErrorApiActivity
import com.raven.home.presentation.view.detail.NewsDetailActivity
import com.raven.home.presentation.viewmodel.NewsCatalogViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class NewsCatalogFragment : BaseFragment<NewsCatalogFragmentBinding>(),
    NewsAdapter.ItemClickListener {

    private val viewModel by lazy { ViewModelProvider(this)[NewsCatalogViewModel::class.java] }

    override fun layout() = R.layout.news_catalog_fragment

    override fun init() {
        setObserver()
        setListener()
        viewModel.getNews()
    }

    private fun setObserver() {
        with(viewModel) {
            news.observe(viewLifecycleOwner) {
                binding.newsRecycler.apply {
                    visibleOrGone(true)
                    adapter =
                        NewsAdapter(this@NewsCatalogFragment).apply {
                            submitList(it)
                        }
                    layoutManager = LinearLayoutManager(requireContext())
                    isNestedScrollingEnabled = false
                    isFocusable = false
                }
            }
            errorResponse.observe(viewLifecycleOwner) {
                binding.errorContainer.visibleOrGone(it)
            }
            unauthorizeResponse.observe(viewLifecycleOwner) {
                ErrorApiActivity.start(requireContext())
            }
            loading.observe(viewLifecycleOwner) {
                binding.loading.visibleOrGone(it)
            }
        }
    }

    private fun setListener() {
        binding.apply {
            errorRetry.setOnClickListener {
                viewModel.getNews()
            }
        }
    }

    override fun onItemClick(news: NewsDTO) {
        NewsDetailActivity.start(requireContext(), news)
    }
}
