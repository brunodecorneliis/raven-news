package com.raven.home.presentation.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.raven.home.R
import com.raven.home.databinding.ViewholderArticleBinding
import com.raven.home.domain.model.NewsDTO

class NewsAdapter(
    private val onItemClickListener: ItemClickListener
) : ListAdapter<NewsDTO, BindingViewHolder<NewsDTO, ViewholderArticleBinding>>(
    DiffCallback()
) {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): BindingViewHolder<NewsDTO, ViewholderArticleBinding> {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ViewholderArticleBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding, onItemClickListener)
    }

    override fun onBindViewHolder(
        holder: BindingViewHolder<NewsDTO, ViewholderArticleBinding>, position: Int
    ) {
        holder.bind(getItem(position))
    }

    private class ViewHolder(
        binding: ViewholderArticleBinding, private val listener: ItemClickListener
    ) : BindingViewHolder<NewsDTO, ViewholderArticleBinding>(binding) {

        override fun bind(item: NewsDTO) {
            with(binding) {

                root.setOnClickListener {
                    listener.onItemClick(item)
                }

                title.text = item.title
                description.text = item.abstract

                if (item.media.isNotEmpty()) {
                    Glide.with(image).load(item.media.first().mediaMetadata.first().url)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(
                                e: GlideException?,
                                model: Any,
                                target: Target<Drawable>,
                                isFirstResource: Boolean
                            ): Boolean {
                                image.setImageResource(R.drawable.note_off_outline)
                                return false
                            }

                            override fun onResourceReady(
                                resource: Drawable,
                                model: Any,
                                target: Target<Drawable>,
                                dataSource: DataSource,
                                isFirstResource: Boolean
                            ): Boolean {
                                return false
                            }
                        }).into(image)
                }
            }
        }
    }

    private class DiffCallback : DiffUtil.ItemCallback<NewsDTO>() {
        override fun areItemsTheSame(
            oldItem: NewsDTO, newItem: NewsDTO
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: NewsDTO, newItem: NewsDTO
        ): Boolean {
            return oldItem == newItem
        }
    }

    interface ItemClickListener {
        fun onItemClick(news: NewsDTO)
    }
}