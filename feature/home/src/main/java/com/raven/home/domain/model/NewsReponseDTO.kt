package com.raven.home.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsReponseDTO(
    @SerializedName("status")
    val status: String?,
    @SerializedName("results")
    val results: List<NewsDTO>,
) : Parcelable