package com.raven.home.domain

import com.raven.home.domain.model.NewsReponseDTO
import retrofit2.Call

interface HomeDataSource {

    suspend fun getNews(): Call<NewsReponseDTO>
}
