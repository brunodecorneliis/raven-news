package com.raven.home.data.remote.service

import com.raven.home.domain.model.NewsReponseDTO
import retrofit2.Call
import retrofit2.http.GET

interface HomeService {

    //TODO("Correctly apply the Path and its answers. The API Key is provided in your PDF document")

    @GET("emailed/7.json")
    fun getNews(): Call<NewsReponseDTO>
}
