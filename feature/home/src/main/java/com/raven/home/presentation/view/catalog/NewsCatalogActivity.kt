package com.raven.home.presentation.view.catalog

import android.content.Context
import android.content.Intent
import com.raven.core.bases.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsCatalogActivity : BaseActivity() {

    override fun init() {
        hideBackButton()
        replaceFragment(NewsCatalogFragment())
    }

    companion object {
        fun start(
            context: Context
        ) {
            with(Intent(context, NewsCatalogActivity::class.java)) {
                context.startActivity(this)
            }
        }
    }
}