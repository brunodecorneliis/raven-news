package com.raven.home.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class NewsMediaDTO(
    @SerializedName("type")
    val type: String?,
    @SerializedName("media-metadata")
    val mediaMetadata: List<MediaMetadaDTO>
) : Parcelable