package com.raven.home.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.raven.core.Constants.OK
import com.raven.core.launch
import com.raven.home.data.HomeRepository
import com.raven.home.domain.model.NewsDTO
import com.raven.home.domain.model.NewsReponseDTO
import dagger.hilt.android.lifecycle.HiltViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

@HiltViewModel
class NewsCatalogViewModel @Inject constructor(private val repository: @JvmSuppressWildcards HomeRepository) :
    ViewModel() {

    private val _news = MutableLiveData<List<NewsDTO>>()
    val news: LiveData<List<NewsDTO>> get() = _news

    private val _errorResponse = MutableLiveData<Boolean>()
    val errorResponse: LiveData<Boolean> get() = _errorResponse

    private val _unauthorizeResponse = MutableLiveData<Boolean>()
    val unauthorizeResponse: LiveData<Boolean> get() = _unauthorizeResponse

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean> get() = _loading

    fun getNews() {
        launch {
            _loading.postValue(true)
            _errorResponse.postValue(false)
            try {
                repository.getNews().enqueue(object : Callback<NewsReponseDTO> {
                    override fun onResponse(
                        call: Call<NewsReponseDTO>, response: Response<NewsReponseDTO>
                    ) {
                        _loading.postValue(false)

                        if (response.isSuccessful && response.body()?.status == OK) {
                            _news.postValue(response.body()?.results)
                        } else {
                            _errorResponse.postValue(true)
                        }
                    }

                    override fun onFailure(call: Call<NewsReponseDTO>, t: Throwable) {
                        _loading.postValue(false)
                        _errorResponse.postValue(true)
                    }
                })
            } catch (e: Exception) {
                _loading.postValue(false)
                _unauthorizeResponse.postValue(true)
            }
        }
    }
}
