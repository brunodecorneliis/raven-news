package com.raven.home.data

import com.raven.home.data.remote.service.HomeService
import com.raven.home.domain.HomeDataSource
import com.raven.home.domain.model.NewsReponseDTO
import kotlinx.coroutines.flow.Flow
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class HomeRepository @Inject constructor(private val service: @JvmSuppressWildcards HomeService) : HomeDataSource {

    override suspend fun getNews(): Call<NewsReponseDTO> {
        return service.getNews()
    }
}
